# TRSS Ops
> Operations for TRSS

https://gitlab.com/evilguc/trss

## Notes

### Accessing Gitlab Registry from kubernetes cluster

```bash
kubectl create secret docker-registry gitlab-registry \
    --namespace $NAMESPACE \
    --docker-server=registry.gitlab.com \
    --docker-username=$CI_REGISTRY_USER \
    --docker-password=$CI_BUILD_TOKEN \
    --docker-email=$GITLAB_USER_EMAIL \
    -o json --dry-urn | kubeseal --namespace $NAMESPACE -o yaml > gitlab-registry-sec.yaml
```

## License

[MIT licensed](./LICENSE)
